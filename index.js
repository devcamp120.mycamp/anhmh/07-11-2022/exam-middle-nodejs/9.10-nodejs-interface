//Câu lệnh này tương tự import express from 'express'; 
//Dùng để import thư viện vào project
const { response } = require("express");
const express = require("express");
const path = require("path");


//Khởi tạo app express
const app = express();

//Khai báo cổng của project
const port = 8000;

//Viết middleware console ra thời gian hiện tại mỗi lần chạy
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
})
app.use((request, response, next) => {
    console.log("URL",(__dirname + '/views/index.html'));
    next();
})

app.use(express.static(__dirname + "/views"))

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Call back function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi 
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/index.html'))
})



//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + " " + port);
})